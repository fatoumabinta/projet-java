package ulco.cardGame.common.players;

import ulco.cardGame.common.games.BoardPlayer;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;

import java.util.List;

public class CardPlayer extends BoardPlayer {
    /**
     * attribut de la classe
     */
    private List<Card> cards;

    /**
     * constructeur de la classe BoardPlayer
     * @param name
     */
    public CardPlayer(String name){
        super(name);
    }

    /**
     * methode qui retourne le score du joueur
     * @return
     */
    public Integer getScore(){
        return this.score;
    }

    /**
     * methode qui permet d'ajouter une carte dans la main de l'utilisateur et mettre a jour son score
     * @param component
     */
    public void addComponent(Component component){
        Card card=(Card)component;
        card.setHidden(false);
        if(this.cards==null){
            List<Card> tmp=new ArrayList<>();
            tmp.add(card);
            this.cards=tmp;
        }
        else this.cards.add(card);
        this.score+=1;
    }

    /**
     * methode qui supprime  une carte de la main de l'utilisateur et met a jour son score
     * @param component
     */
    public void removeComponent(Component component){
        this.cards.remove(component);
        this.score-=1;
    }

    /**
     * methode qui permet de jouer la premiere carte
     * @return
     */
    /*public Card play(){
        Card cp= this.cards.get(0);
        cp.setHidden(true);
        this.removeComponent(cp);
        return cp;
    }*/
    public void play(Socket socket) throws IOException {
        //recuperer la carte
        Card cp=this.cards.get(0);
        cp.setHidden(true);
        //Envoyer la carte jouer
        ObjectOutputStream oos=new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject(cp);
    }

    /**
     * methode qui retourne l'ensemble des cartes de la main de l'utilisateur
     * @return
     */
    public List<Component> getComponents(){
        return new ArrayList<>(this.cards);
    }

    /**
     * methode qui permet de melanger les cartes de la main de l'utilisateur
     */
    public void shuffleHand(){
            Collections.shuffle(this.cards);
    }

    /**
     * methode qui supprime l'ensemble des cartes de la main de l'utilisateur
     */
    public void clearHand(){
        this.cards.clear();
    }

    /**
     * methode qui affiche les informations du joueur
     * @return
     */
    public String toString(){
       return  super.toString();
    }

    /**
     * afficher les informations des composant de la main du joueur
     */
    public void displayHand() {
        System.out.println("----------------");
        System.out.printf("Player %s has %d same card(s) of value %d\n",this.getName(),this.getSpecificComponents(Card.class).size(),this.getScore());
        System.out.println("---------------");
    }

    /**
     * retourner une liste de composant specific
     *
     * @param classType
     * @return
     */
    public List<Component> getSpecificComponents(Class classType) {
        if(classType==Card.class) return new ArrayList<>(this.cards);
        return null;
    }
}
