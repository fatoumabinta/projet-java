package ulco.cardGame.common.interfaces;

import ulco.cardGame.common.games.components.Component;

import java.io.Serializable;
import java.util.List;

public interface Board extends Serializable {
    /**
     * nettoyer le Board
     */
    void clear();

    /**
     * ajouter un composant
     * @param component
     */
    void addComponent(Component component);

    /**
     * retourner une liste de composant
     * @return
     */
    List<Component> getComponents();

    /**
     * retourner une liste de composant specific
     * @param classType
     * @return
     */
    List<Component> getSpecificComponents(Class classType);

    /**
     * afficher les informations de Board
     */
    void displayState();
}
