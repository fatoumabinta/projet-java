package ulco.cardGame.common.games;

import ulco.cardGame.common.interfaces.Player;

public abstract class BoardPlayer implements Player {
    /**
     * attributs de la classe
     */
    protected String name;
    protected boolean  playing;
    protected Integer score;

    /**
     * constructeur de la classe
     * @param name
     */
    public BoardPlayer(String name){
        this.name=name;
        this.score=0;
        this.playing=false;
    }

    /**
     * methode qui specifie si le joueur peut jouer ou non
     * @param playing
     */
    public void canPlay(boolean playing){
        this.playing=playing;
    }

    /**
     * methode qui retourne le nom de l'utilisateur
     * @return
     */
    public String getName(){
        return this.name;
    }

    /**
     * methode qui recupere le statut de jeu de l'utilisateur
     * @return
     */
    public boolean isPlaying(){
        return this.playing;
    }

    public Integer getScore(){
        return this.score;
    }

    /**
     * mehtode qui affiche les informations du joueur
     * @return
     */
    public String toString(){
        return String.format("%s {name='%s' ,  Score=%d}",this.getClass().getSimpleName(),this.name,this.score);
    }
}
