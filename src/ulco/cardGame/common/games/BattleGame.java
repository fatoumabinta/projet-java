package ulco.cardGame.common.games;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.interfaces.Player;

import java.util.*;

public class BattleGame extends CardGame{
    /**
     * constructeur de la classe BattleGame
     *
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public BattleGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);
    }

    /**
     * methode qui permet d'aller en battle et qui renvoie le joueur gagnan
     *
     * @param playersBattle
     * @return
     */
    /*public Player run(List<Player> playersBattle) {

        Map<Player, Card> playedCardBattle = new HashMap<>();//creation d'un dictionnaire permettant de memoriser le jouer et la carte qu'il a joue
        //premiere manche les joueurs jouent leur carte en face cache
        for (Player py : playersBattle) {//parcourir les joueurs pour jouer
            if (py.isPlaying()) {//verifier si un joueur peut jouer
                Card cd = (Card) py.play();//le joueur joue la carte et on le recupere
                cd.setHidden(false);
                playedCardBattle.put(py, cd);//ajoueter le joueur et sa carte dans le dictionnaire
                System.out.printf("%s has played %s\n", py.getName(), cd.getName());//afficher un message pour dire aux autres joueurs qu'il a joue en donnant son nom et la carte joue
            }
        }
        //Deuxime manche les jouers jouent leur carte en face visible
        for (Player py : playersBattle) {//parcourir les joueurs pour jouer
            if (py.isPlaying()) {//verifier si un joueur peut jouer
                Card cd = (Card) py.play();//le joueur joue la carte et on le recupere
                playedCardBattle.put(py, cd);//ajoueter le joueur et sa carte dans le dictionnaire
                System.out.printf("%s has played %s\n", py.getName(), cd.getName());//afficher un message pour dire aux autres joueurs qu'il a joue en donnant son nom et la carte joue

                if(py.getComponents().isEmpty()){//verifier s'il n'a plus de carte
                    List<Player> dist=new ArrayList<>();//stocker le ou les jouers qui peuvent le fournir de carte
                    for(Player pf:playersBattle){//parcourir les joueurs qui sont en battle
                        if(!playersBattle.contains(pf)){//verifier si c'est n'est pas le joueuer qui demande de carte
                            dist.add(pf);//ajouter le joueur qui peut fournir de carte
                        }
                    }
                    if(!dist.isEmpty()){//verifier si ya au moins un joueurs qui peut fournir de carte
                        if(dist.size()==1){//verifier si c'est un seul joueurs qui peut fournir de carte
                            Card c=(Card)dist.get(0).play();//recuperer la carte

                            py.addComponent(c);//ajouter la carte
                        }else{//si ya plusieurs joueurs qui peut donner leur carte
                            py.addComponent(dist.get(new Random().nextInt(dist.size())).play());//generer un joueur qui va donner la carte
                        }
                    }else{//si ya aucun joueurs qui peut fournir de carte alors on se fournir aux joueur qui sont pas en battle
                        int maxnbCard=0;//sauvgarder le nombre de carte
                        Player fournis=null;//sauvegarder le joueur qui peut donner de carte
                        for(Player p:this.getPlayers()){//parcourir les joueurs
                            if(p.getComponents().size()>maxnbCard){//verifier si c'est lui qui a le plus de carte
                                maxnbCard=p.getComponents().size();//faire la mise a jour de maxnbCard
                                fournis=p;//sauvegarder le joueur qui plus de carte
                            }
                        }
                        //forurinir les cartes aux joueurs de battle
                        for(Player pb:playersBattle){//parcouir les joueurs de battle
                            Card c=(Card)fournis.play();//recuperer la carte

                            pb.addComponent(c);//
                        }
                    }

                }
            }
        }

        //Verifier quel joueur doit remporter la battle
        List<Player> gagnantBAttle = new ArrayList<>();//creation d'une liste de joueurs de gagant
        int MaxCard = 0;//variable permettant de sauvegarder la  plus haute carte
        for (Map.Entry<Player, Card> entry : playedCardBattle.entrySet()) {//parcourir les cartes jouees
            Player player = entry.getKey();//recuperer le joueur
            Card card = entry.getValue();//recuperer sa carte
            if (card.getValue() >= MaxCard) {//verifier est ce la plus haute carte
                if (MaxCard == card.getValue()) gagnantBAttle.add(player);//verifier s'il est egal a la plus haute carte pour l'ajouter a la liste de gagnant
                else {
                    MaxCard = card.getValue();//sauvegarder la plus haute carte car elle a changé
                    if (!gagnantBAttle.isEmpty()) {
                        gagnantBAttle.clear();//supprimer les joueurs si c'est pas vide parce que c'est pas eux qui possedent la plus haute carte
                        gagnantBAttle.add(player);//ajouter le jouer a la liste gagnant
                    }
                }

            }
        }

        //ramasser les cartes gagne par le vainqueur de round
        if (gagnantBAttle.size() == 1) {//verifier si on a un seul gagnant
            for (Map.Entry<Player, Card> entry : playedCardBattle.entrySet()) {//parcourir les cartes pour les ramasser
                gagnantBAttle.get(0).addComponent(entry.getValue());//ajouter la carte a la liste de la carte du jouer qui a gagné
                //modifier le statut du joueur s'il n'a plus de carte
                if (entry.getKey().getComponents().isEmpty()) entry.getKey().canPlay(false);
            }
            return gagnantBAttle.get(0);//retourner le vainquer de BAttleGame

        }

        return run(gagnantBAttle);//aller en batltle s'il a egalite ente des joueurs
    }*/
}
