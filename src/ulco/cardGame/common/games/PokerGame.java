package ulco.cardGame.common.games;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class PokerGame extends BoardGame{
    /**
     * attributs de la classe
     */
    private List<Card>cards;
    private List<Coin> coins;
    private Integer maxRounds;
    private Integer numberOfRounds=0;

    /**
     * constructeur de la classe
     * @param name
     * @param maxPlayers
     * @param maxRounds
     * @param filename
     */
    public PokerGame(String name,Integer maxPlayers,Integer maxRounds,String filename){
        super(name,maxPlayers,filename);
       this.maxRounds=maxRounds;

       //this.initialize(filename);
    }


    /**
     * Initialize the whole game using a parameter file
     *
     * @param filename
     */
    public void initialize(String filename) {
        try {
            File cardFile = new File(filename);
            Scanner myReader = new Scanner(cardFile);
            this.cards=new ArrayList<>();
            this.coins=new ArrayList<>();
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String []dataLine=data.split(";");//decouper la ligne en des mots separant par des ; et mettre dans le tableau
                //Verifier quel composant on doit creer et ajouter a l'une de nos liste cards ou coins
                if (dataLine[0].equals("Card"))  this.cards.add(new Card(dataLine[1], Integer.parseInt(dataLine[2]), false));//instancier la carte et l'ajouter
                else this.coins.add(new Coin(dataLine[1], Integer.parseInt(dataLine[2])));//inscier le jeton et continuer

            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Erreur de lecture");
            e.printStackTrace();
        }
    }


    /**
     * methode qui fait jouer au pocker et qui renvoie le gagant
     * @return
     */
    /*public Player run() {

        //distribuer aux joueurs le meme nombre de jeton
        for (Player p : this.getPlayers()) {
            for (Coin co : this.coins) {
                p.addComponent(co);
            }
        }
        //Debut des rounds
        while (!this.end()) {
            //melanger les cartes aleatoirement
            Collections.shuffle(this.cards);
            //Distribution une a une chacun des joueurs jusqu'a ce que chaque joueur en possede 3
            final int nbCardDist = 3;
            Player WinJeton = null;
            for (int i = 0; i < nbCardDist; i++) {
                for (Player p : this.getPlayers()) {
                    Card card = this.cards.get(0);//prendre  la carte de haut sur la liste de carte
                    p.addComponent(card);//donner la carte au joueur
                    this.cards.remove(card);//supprimer la carte de la liste
                }
            }
            //En fonction de leur jeu en main, chaque joueur mise ici un jeton de valeur de son choix
            for (Player p : this.getPlayers()) {//parcourir les joueurs
                if(p.isPlaying()){//verifier s'il peut jouer ou non
                    p.displayHand();//afficher les info du jeu de la main de l'utilsateur
                    Coin playCoin = (Coin) p.play();//miser un jeton selon la couleur du jeton
                    if (playCoin != null)
                        this.getBoard().addComponent(playCoin);//mettre le jeton joue sur le plateau de jeu
                    else{
                        p.canPlay(false);//changer son staut de jeu il doit plus continuer la manche
                    }
                }

            }
            // 3 nouvelles cartes (du haut de la pile) sont ensuite dévoilées à l’ensemble des joueurs et posées sur
            //le plateau de jeu ;
            for (int i = 0; i < nbCardDist; i++) {
                Card card = this.cards.get(0);//prendre  la carte de haut sur la liste de carte
                card.setHidden(true);//changer la visibilite de la carte
                this.getBoard().addComponent(card);//mettre la carte sur le jeu de plateau
                this.cards.remove(card);//supprimer la carte de la liste
            }
            //Les joueurs peuvent en fonction de leur main, c’est-à-dire maintenant le jeu disponible et leur main
            //(cartes posées sur le plateau et cartes en main), miser de nouveau un jeton. Un joueur qui mise un
            //jeton d’une grande valeur est généralement confiant de gagner (ou alors il bluff très bien) ;
            for (Player p : this.getPlayers()) {//parcourir les joueurs
                if(p.isPlaying()){
                    p.displayHand();//afficher les info du jeu de la main de l'utilsateur
                    Coin playCoin = (Coin) p.play();//miser un jeton selon la couleur du jeton
                    if (playCoin != null)
                        this.getBoard().addComponent(playCoin);//mettre le jeton joue sur le plateau de jeu
                    else p.canPlay(false);//modifier son statut puisque qu'il ne mise pas
                }

            }
            //Une fois le dernier tour de mise de jetons réalisé par les joueurs, chaque joueur dévoile son jeu. Le
            //joueur ayant le plus grand nombre de cartes identiques (et de plus grande hauteur si égalité) gagne
            //le round. Il gagne l’intégralité de la mise disponible sur le plateau.C’est-à-dire que l’ensemble des
            //jetons posés sur le plateau lui sont maintenant associés ;
            for (Player p : this.getPlayers()) {//parcourir les joueurs
                for (Component comp : p.getSpecificComponents(Card.class)) {//parcourir ses cartes
                    ((Card) comp).setHidden(true);//changer la visibilite
                }
            }
            int cptPlayer=0;//compter le nombre de joueur qui peut continuer la manche
            for(Player p:this.getPlayers()){//parcourir les joueurs
                if(p.isPlaying()){//s'il peut jouer le compter et consider qu'il est gagnant
                    cptPlayer+=1;//increment la variable
                    WinJeton=p;//sauvegarder le joueur comme gagnant s'il est le seul a continuer la manche
                }
            }
            Map<Player, Integer> PlayerNumberCard = new HashMap<>();//variable permettant des sauvegarder joueur et le nombre de carte identique
            if(cptPlayer!=1){

                for (Player p : this.getPlayers()) {//parcourir les les joueurs
                    if(p.isPlaying()){
                        Map<Card, Integer> NumberOFNameCard = new HashMap<>();//variable pour stoker la carte et le nombre de fois qu'il exite
                        for (Component comp : p.getSpecificComponents(Card.class)) {//parcourir les cartes du joueur
                            boolean exist = false;
                            for (Map.Entry<Card, Integer> entry : NumberOFNameCard.entrySet()) {
                                if (comp.getValue().equals(entry.getKey().getValue())) {//verifier si cette carte correspond a l' un des cartes de la liste de carte identique pour le nombre de carte identique a la carte
                                    entry.setValue(entry.getValue() + 1);//augmenter sa quantite
                                    exist = true;//signale qu'on a trouve
                                    break;//sortir de la boucle si on a trouve
                                }
                            }
                            if (!exist)  NumberOFNameCard.put((Card) comp, 1);//ajouter cette carte si elle n'existe pas a la liste de carte identique
                        }
                        int maxnbCard = 0;//sauvegarder le max du nombre identique de carte trouve
                        for (Map.Entry<Card, Integer> entry : NumberOFNameCard.entrySet()) {//parcourir les cartes range pour trouver le plus grand nombre de de carte identique
                            if (entry.getValue() > maxnbCard) ;
                            maxnbCard = entry.getValue();//modifier le maxnbCard si cette nombre de carte identique est plus grand que le precedent
                        }
                        PlayerNumberCard.put(p, maxnbCard);//ajouter le joueur et son plus grand nombre de carte identique
                    }

                }
                int maxnbCardPlayer = 0;//sauvegarder le max de celui qui a le plus grand nombre de carte identique
                List<Player> gagnant = new ArrayList<>();//sauvegarder un gagnat ou plusieurs gagnat si ya égalité
                for (Map.Entry<Player, Integer> entry : PlayerNumberCard.entrySet()) {//parcourir les joueurs et leur plus grand nombre de carte identique
                    Player player = entry.getKey();//recuperer le joueur
                    Integer nbCard = entry.getValue();//recupere son nombre de carte identique
                    if (nbCard >= maxnbCardPlayer) {//verifier si c'est le joueur qui a le plus grand ou egal  nombre de carte identique
                        if (nbCard > maxnbCardPlayer) {//Verifier si elle plus grande
                            maxnbCardPlayer = nbCard;//modifier maxnbCardPlyer car elle n'est plus grande
                            gagnant.clear();//nettoyer la liste de gagnant car c'est pas eux qui detiennent le plus grand nombre de carte identique
                        }
                        gagnant.add(player);//ajouter le joueur a la liste de gagnant
                    }
                }

                //Verifier si En cas d'egalité
                if (gagnant.size() > 1) {//verifier si on a plusieurs gagnant egaux
                    List<Player> gagnantAleatoire = new ArrayList<>();//liste de joueurs si ya egalite pour faire aleatoire un gagant
                    int CardHaute = 0;//sauvegarder la plus haute carte du joueur gagnant
                    for (Player p : gagnant) {//parcourir les joueurs gagnant
                        if(p.isPlaying()){
                            int CardHautePlayer = 0;//sauvegarder la carte la plus haute du joueurs
                            for (Component comp : p.getSpecificComponents(Card.class)) {//parcourir les cartes du joueurs
                                if (comp.getValue() > CardHaute)  CardHautePlayer = comp.getValue();//verifier est ce la plus haute carte si oui la modidier
                            }
                            if (CardHaute <= CardHautePlayer) {//verifier si elle est la plus haute carte ou egal a la carte la plus haute du gagnant
                                if (CardHaute < CardHautePlayer) {//verifier si elle est superieur
                                    CardHaute = CardHautePlayer;//changer sa valeur
                                    gagnantAleatoire.clear();//nettoyer le tableau
                                }
                                gagnantAleatoire.add(p);//ajouter le gagnant
                            }
                        }
                    }
                    //verifier si on a un seul gagnant ou plusieurs egaux
                    if (gagnantAleatoire.size() > 1)
                        WinJeton = gagnantAleatoire.get(new Random().nextInt(gagnantAleatoire.size()));//generer le gagnant aleatoire entre les joueurs et le stocker
                    else
                        WinJeton = gagnantAleatoire.get(0);//si ya un seul gagnantAleatoire on le recupere pour le sauvegarder
                } else WinJeton = gagnant.get(0);//si ya un seul gagnantAleatoire on le recupere pour le sauvegarder
            }


            for (Map.Entry<Player, Integer> entry : PlayerNumberCard.entrySet()) {//parcourir les joueurs pour afficher leur nombre de carte identique

                    System.out.printf("Player %s has  %d same card(s)\n", WinJeton.getName(), entry.getValue());

            }
           //recuperer tous les jeton du plateau
            for(Component jeton:this.getBoard().getSpecificComponents(Coin.class)){//parcourir les jeton posze sur le plateau
                WinJeton.addComponent(jeton);//ajouter le jeton au joueur gagnant
            }
            for (Player p : this.getPlayers()) {//parcourir  les joueurs
                for (Component comp : p.getSpecificComponents(Card.class)) {//parcourir les cartes du joeurs pour les reprendre
                    this.cards.add((Card) comp);//ajouter a la liste de carte
                }
                p.clearHand();//nettoyer sa main            }
            }
            for (Component comp : this.getBoard().getSpecificComponents(Card.class)) {//recuperer les cartes posee sur le plateau
                this.cards.add((Card) comp);//prendre la carte
            }
            this.getBoard().clear();//nettoyer le plateau
            for(Player p:this.getPlayers()){//parcourir les joueurs
                if(!p.isPlaying()) p.canPlay(true);//changer son statut pour continuer la manche s'il n'a pas mise la derniere manche
            }
            this.numberOfRounds += 1;//incrmenter le round
            System.out.printf("End of the round number %d of %d\n", this.numberOfRounds , this.maxRounds);

        }
        int ScoreWinner = 0;
        Player Winner = null;
        for (Player p : this.getPlayers()) {
            if (p.getScore() > ScoreWinner) {
                ScoreWinner = p.getScore();
                Winner = p;
            }
        }
        return Winner;
    }*/
    /**
     * Specify if the Game has end or not
     */
    public boolean end() {
        //verifier si on aatteint la fin du round
        if(this.maxRounds==this.numberOfRounds){
            this.endGame=true;
            return true;
        }
        //verifier si ya un jouer qui a gagne tous les jetons
        int cpt=0;
        for(Player p:this.getPlayers()){
            if(!p.getSpecificComponents(Coin.class).isEmpty()){
                cpt+=1;
                if(cpt==2) return false;
            }
        }
        this.endGame=true;
        return true;
    }
    /**
     * return winner players
     *
     * @param playersSocket
     * @return
     */
    public Player run(Map<Player, Socket> playersSocket) throws IOException, ClassNotFoundException {
        System.out.println(this.getPlayers());
        //distribuer aux joueurs le meme nombre de jeton
        for (Player p : this.getPlayers()) {
            for (Coin co : this.coins) {
                p.addComponent(co);
            }
        }
        //Envoyer un message a tous les joueurs l'info du jeu de Poker
        for(Socket socket:playersSocket.values()){
            ObjectOutputStream oos=new ObjectOutputStream (socket.getOutputStream());
            oos.writeObject((Game)this);
        }
        //Debut des rounds
        while (!this.end()) {
            //melanger les cartes aleatoirement
            Collections.shuffle(this.cards);
            //Distribution une a une chacun des joueurs jusqu'a ce que chaque joueur en possede 3
            final int nbCardDist = 3;
            Player WinJeton = null;
            for (int i = 0; i < nbCardDist; i++) {
                for (Player p : this.getPlayers()) {
                    Card card = this.cards.get(0);//prendre  la carte de haut sur la liste de carte
                    p.addComponent(card);//donner la carte au joueur
                    this.cards.remove(card);//supprimer la carte de la liste
                }
            }
            for(int mise=1;mise<=2;mise++){
                Map<Player,Coin>playedCoin=new HashMap<>();
                //En fonction de leur jeu en main, chaque joueur mise ici un jeton de valeur de son choix
                for(Map.Entry<Player,Socket>ps: playersSocket.entrySet()){//parcourir les joueurs et leurs socket de jeu
                    //recuper le joueur et son socket
                    Player player=ps.getKey();
                    Socket socket=ps.getValue();
                    if(player.isPlaying()){//verifier s'il peut jouer
                        //Envoyer un message a tous les autres joueurs d'attendre que le jouer en cours joue sa carte
                        for(Socket socketPlayer:playersSocket.values()){
                            if(socketPlayer.equals(socket)) continue;
                            ObjectOutputStream oosPlayString=new ObjectOutputStream(socketPlayer.getOutputStream());
                            oosPlayString.writeObject("Waiting for "+player.getName()+" to play...");
                        }
                        //Envoyer un message au jouer courant de jouer
                        Map<String,PokerGame>playUser=new Hashtable<>();
                        playUser.put("["+player.getName()+"] you have to play",this);
                        ObjectOutputStream oosPlayString=new ObjectOutputStream(socket.getOutputStream());
                        oosPlayString.writeObject(playUser);
                        //recuperer le composant carte qu'il a joue
                        ObjectInputStream oisCoin=new ObjectInputStream(socket.getInputStream());
                        Coin coinUser=(Coin)oisCoin.readObject();
                        if (coinUser != null) {
                            //mettre le jeton  dans le PokerBoard
                            this.getBoard().addComponent(coinUser);
                            playedCoin.put(player, coinUser);
                        }else{
                            player.canPlay(false);//changer son staut de jeu il doit plus continuer la manche
                        }

                        //supprimer la carte jouer de sa liste de carte
                        player.removeComponent(coinUser);
                        //Envoyer un message a tous les autres la carte que le joueur avec son nom
                        for(Socket socketPlayer:playersSocket.values()){
                            if(socketPlayer.equals(socket)) continue;
                            ObjectOutputStream oosPlayStringCard=new ObjectOutputStream(socketPlayer.getOutputStream());
                            oosPlayStringCard.writeObject("Player "+player.getName()+" has played "+coinUser.getName());
                        }
                    }
                }
                if(mise==1){
                    // 3 nouvelles cartes (du haut de la pile) sont ensuite dévoilées à l’ensemble des joueurs et posées sur
                    //le plateau de jeu ;
                    for (int i = 0; i < nbCardDist; i++) {
                        Card card = this.cards.get(0);//prendre  la carte de haut sur la liste de carte
                        card.setHidden(true);//changer la visibilite de la carte
                        this.getBoard().addComponent(card);//mettre la carte sur le jeu de plateau
                        this.cards.remove(card);//supprimer la carte de la liste
                    }
                }
                //preparer le messaage de l'etat du round
                String msgRound="-----------Board State-------------\n";
                for(Map.Entry<Player,Coin>pc: playedCoin.entrySet()){
                    msgRound+=String.format("%s played by %s\n",pc.getValue().getName(),pc.getKey().getName());
                }
                msgRound+="-----------------------------------\n";
                //Envoyer le mesage de l'etat de round a tous les joueurs
                for(Socket socketPlayer:playersSocket.values()){
                    ObjectOutputStream oosMsgRound=new ObjectOutputStream(socketPlayer.getOutputStream());
                    oosMsgRound.writeObject(msgRound);
                }
            }
            //Une fois le dernier tour de mise de jetons réalisé par les joueurs, chaque joueur dévoile son jeu. Le
            //joueur ayant le plus grand nombre de cartes identiques (et de plus grande hauteur si égalité) gagne
            //le round. Il gagne l’intégralité de la mise disponible sur le plateau.C’est-à-dire que l’ensemble des
            //jetons posés sur le plateau lui sont maintenant associés ;
            for (Player p : this.getPlayers()) {//parcourir les joueurs
                for (Component comp : p.getSpecificComponents(Card.class)) {//parcourir ses cartes
                    ((Card) comp).setHidden(true);//changer la visibilite
                }
            }
            int cptPlayer=0;//compter le nombre de joueur qui peut continuer la manche
            for(Player p:this.getPlayers()){//parcourir les joueurs
                if(p.isPlaying()){//s'il peut jouer le compter et consider qu'il est gagnant
                    cptPlayer+=1;//increment la variable
                    WinJeton=p;//sauvegarder le joueur comme gagnant s'il est le seul a continuer la manche
                }
            }
            Map<Player, Integer> PlayerNumberCard = new HashMap<>();//variable permettant des sauvegarder joueur et le nombre de carte identique
            if(cptPlayer!=1){

                for (Player p : this.getPlayers()) {//parcourir les les joueurs
                    if(p.isPlaying()){
                        Map<Card, Integer> NumberOFNameCard = new HashMap<>();//variable pour stoker la carte et le nombre de fois qu'il exite
                        for (Component comp : p.getSpecificComponents(Card.class)) {//parcourir les cartes du joueur
                            boolean exist = false;
                            for (Map.Entry<Card, Integer> entry : NumberOFNameCard.entrySet()) {
                                if (comp.getValue().equals(entry.getKey().getValue())) {//verifier si cette carte correspond a l' un des cartes de la liste de carte identique pour le nombre de carte identique a la carte
                                    entry.setValue(entry.getValue() + 1);//augmenter sa quantite
                                    exist = true;//signale qu'on a trouve
                                    break;//sortir de la boucle si on a trouve
                                }
                            }
                            if (!exist)  NumberOFNameCard.put((Card) comp, 1);//ajouter cette carte si elle n'existe pas a la liste de carte identique
                        }
                        int maxnbCard = 0;//sauvegarder le max du nombre identique de carte trouve
                        for (Map.Entry<Card, Integer> entry : NumberOFNameCard.entrySet()) {//parcourir les cartes range pour trouver le plus grand nombre de de carte identique
                            if (entry.getValue() > maxnbCard) ;
                            maxnbCard = entry.getValue();//modifier le maxnbCard si cette nombre de carte identique est plus grand que le precedent
                        }
                        PlayerNumberCard.put(p, maxnbCard);//ajouter le joueur et son plus grand nombre de carte identique
                    }

                }
                int maxnbCardPlayer = 0;//sauvegarder le max de celui qui a le plus grand nombre de carte identique
                List<Player> gagnant = new ArrayList<>();//sauvegarder un gagnat ou plusieurs gagnat si ya égalité
                for (Map.Entry<Player, Integer> entry : PlayerNumberCard.entrySet()) {//parcourir les joueurs et leur plus grand nombre de carte identique
                    Player player = entry.getKey();//recuperer le joueur
                    Integer nbCard = entry.getValue();//recupere son nombre de carte identique
                    if (nbCard >= maxnbCardPlayer) {//verifier si c'est le joueur qui a le plus grand ou egal  nombre de carte identique
                        if (nbCard > maxnbCardPlayer) {//Verifier si elle plus grande
                            maxnbCardPlayer = nbCard;//modifier maxnbCardPlyer car elle n'est plus grande
                            gagnant.clear();//nettoyer la liste de gagnant car c'est pas eux qui detiennent le plus grand nombre de carte identique
                        }
                        gagnant.add(player);//ajouter le joueur a la liste de gagnant
                    }
                }
                //Verifier si En cas d'egalité
                if (gagnant.size() > 1) {//verifier si on a plusieurs gagnant egaux
                    List<Player> gagnantAleatoire = new ArrayList<>();//liste de joueurs si ya egalite pour faire aleatoire un gagant
                    int CardHaute = 0;//sauvegarder la plus haute carte du joueur gagnant
                    for (Player p : gagnant) {//parcourir les joueurs gagnant
                        if(p.isPlaying()){
                            int CardHautePlayer = 0;//sauvegarder la carte la plus haute du joueurs
                            for (Component comp : p.getSpecificComponents(Card.class)) {//parcourir les cartes du joueurs
                                if (comp.getValue() > CardHaute)  CardHautePlayer = comp.getValue();//verifier est ce la plus haute carte si oui la modidier
                            }
                            if (CardHaute <= CardHautePlayer) {//verifier si elle est la plus haute carte ou egal a la carte la plus haute du gagnant
                                if (CardHaute < CardHautePlayer) {//verifier si elle est superieur
                                    CardHaute = CardHautePlayer;//changer sa valeur
                                    gagnantAleatoire.clear();//nettoyer le tableau
                                }
                                gagnantAleatoire.add(p);//ajouter le gagnant
                            }
                        }
                    }
                    //verifier si on a un seul gagnant ou plusieurs egaux
                    if (gagnantAleatoire.size() > 1)
                        WinJeton = gagnantAleatoire.get(new Random().nextInt(gagnantAleatoire.size()));//generer le gagnant aleatoire entre les joueurs et le stocker
                    else
                        WinJeton = gagnantAleatoire.get(0);//si ya un seul gagnantAleatoire on le recupere pour le sauvegarder
                } else WinJeton = gagnant.get(0);//si ya un seul gagnantAleatoire on le recupere pour le sauvegarder
            }


            //recuperer tous les jeton du plateau
            for(Component jeton:this.getBoard().getSpecificComponents(Coin.class)){//parcourir les jeton posze sur le plateau
                WinJeton.addComponent(jeton);//ajouter le jeton au joueur gagnant
            }
            //Envoyer un message a tous les joueurs le gagnat du round
            String msgWinRound=String.format("Player :%s won the round ",WinJeton.toString());
            for(Socket socketPlayer:playersSocket.values()){
                ObjectOutputStream oosMsgWinRound=new ObjectOutputStream(socketPlayer.getOutputStream());
                oosMsgWinRound.writeObject(msgWinRound);
            }
            for (Player p : this.getPlayers()) {//parcourir  les joueurs
                for (Component comp : p.getSpecificComponents(Card.class)) {//parcourir les cartes du joeurs pour les reprendre
                    this.cards.add((Card) comp);//ajouter a la liste de carte
                }
                p.clearHand();//nettoyer sa main            }
            }
            for (Component comp : this.getBoard().getSpecificComponents(Card.class)) {//recuperer les cartes posee sur le plateau
                this.cards.add((Card) comp);//prendre la carte
            }
            this.getBoard().clear();//nettoyer le plateau
            for(Player p:this.getPlayers()){//parcourir les joueurs
                if(!p.isPlaying() && p.getScore()>0) p.canPlay(true);//changer son statut pour continuer la manche s'il n'a pas mise la derniere manche
            }
            this.numberOfRounds += 1;//incrmenter le round
            System.out.printf("End of the round number %d of %d\n", this.numberOfRounds , this.maxRounds);

        }
        //retourner  le gagant du jeu
        int ScoreWinner = 0;
        Player Winner = null;
        for (Player p : this.getPlayers()) {
            if (p.getScore() > ScoreWinner) {
                ScoreWinner = p.getScore();
                Winner = p;
            }
        }
        return Winner;
    }


    /**
     * methode qui affichent les informations relatives au jeu
     * @return
     */
    public String toString(){
        return this.getClass().getSimpleName()+"{name='"+this.name+"'}";
    }
}
