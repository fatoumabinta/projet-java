package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.PokerBoard;
import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class BoardGame implements Game {
    /**
     * attribut de la classe
     */
    protected String name;
    protected Integer maxPlayers;
    protected List<BoardPlayer> players;
    protected boolean endGame=false;
    protected boolean started=false;
    protected Board board=new PokerBoard();

    /**
     * constructeur de la classe
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public BoardGame(String name, Integer maxPlayers,String filename){
        this.name=name;
        this.maxPlayers=maxPlayers;
        initialize(filename);
    }

    /**
     * methode pour ajouter un joueur
     * @param player
     * @return
     */
    public boolean addPlayer(Socket socket, Player player) throws IOException {
        //verifier si la liste de joueurs est vide
       if(this.players==null){
            List<BoardPlayer> listPlayers=new ArrayList<>();
            player.canPlay(true);
            listPlayers.add((BoardPlayer)player);
            this.players=listPlayers;
            //Envoyer um message au client quand il est bien ajouté
            ObjectOutputStream oos=new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject("Player added into players game List");
            if(this.getPlayers().size()==this.maxNumberOfPlayers()) this.started=true;
            return true;
        }
       //verifier si on a atteint le nombre maximum de joueurs
        if(this.getPlayers().size()<this.maxNumberOfPlayers()){
           for(Player p:this.getPlayers()){
               if(p.getName().equals(player.getName())){
                   //Envoyer um message au client quand le nom qu'il a donné existe
                   ObjectOutputStream oos=new ObjectOutputStream(socket.getOutputStream());
                   oos.writeObject("Username"+p.getName()+" player  already taken");

               }
           }
           player.canPlay(true);
           this.players.add((BoardPlayer) player);
            //Envoyer um message au client quand il est bien ajouté
            ObjectOutputStream oos=new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject("Player added into players game List");
            if(this.getPlayers().size()==this.maxNumberOfPlayers()) this.started=true;
           return true;
        }
        //Envoyer um message au client quand le nombre maximum de joueur est atteint
        ObjectOutputStream oos=new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject("Maximum number of players already reached (max:"+maxPlayers+")");
        return false;
    }

    /**
     * methode pour supprimer un joueur dans la liste des joueurs
     * @param player
     */
    public void removePlayer(Player player){
        this.players.remove(player);
    }

    /**
     * methode pour supprimer tous les joueurs
     */
    public void removePlayers(){
        this.players.clear();
    }

    /**
     * methode pour afficher l'état du'un jeu : afficher tous les joueurs
     */
    public void displayState(){
        System.out.println("--------------------------------");
        System.out.println("-----------GAME STATE-----------");
        System.out.println("--------------------------------");
        for(Player p:this.getPlayers()){
            System.out.printf("%s{name='%s' , score=%d}\n",p.getClass().getSimpleName(),p.getName(),p.getScore());
        }
        System.out.println("--------------------------------");
    }

    /**
     * methode pour savoir si le jeu peut commencer ou non
     * @return
     */
    public boolean isStarted(){
        return this.started;
    }

    /**
     * methode qui retourne le nombre maximum
     * @return
     */
    public Integer maxNumberOfPlayers(){
        return maxPlayers;
    }

    /**
     * methode qui retourne la liste des joueurs
     * @return
     */
    public List<BoardPlayer> getPlayers(){
        List<BoardPlayer>pb=new ArrayList<>();
        for(Player p:this.players) pb.add((BoardPlayer)p);
        return pb;
    }

    public Player getCurrentPlayer(String name){
        for(Player p:this.getPlayers()){
            if(p.getName().equals(name)) return p;
        }
        return null;
    }
    /**
     * methode qui retourne le plateau
     * @return
     */
    public Board getBoard(){
        return this.board;
    }

}
