package ulco.cardGame.server;

import ulco.cardGame.common.games.CardGame;
import ulco.cardGame.common.games.CardPlayer;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.games.PokerPlayer;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.util.Scanner;

/**
 * Server Game management with use of Singleton instance creation
 */
public class GameServer {

    public static void main(String[] args) {

        System.out.println("Here the created games will be launched!");

        String saisie="";
        do{
            System.out.println("Which game do you to play? Card OR Poker");
            saisie=new Scanner(System.in).nextLine();
        }while(!saisie.equals("Poker") && !saisie.equals("Card"));
        if(saisie.equals("Card")){
            // - Create Game Instance
            Game game=new CardGame("Battle",3,"resources\\games\\cardGame.txt");
            // - Add players to game
            Player play1=new CardPlayer("user1");
            Player play2=new CardPlayer("user2");
            Player play3=new CardPlayer("user3");
            Player play4=new CardPlayer("user4");
            game.addPlayer(play1);
            game.addPlayer(play2);
            game.addPlayer(play3);
            game.addPlayer(play4);
            // - Run the Game loop
            Player win= game.run();
            // - Display the winner player
            System.out.println("Le gagant est "+win.toString());
            //nettoryer les cartes de la main du gagnant
            win.clearHand();
            //supprimer les joueurs
            game.removePlayers();
        }
        else{
            // - Create Game Instance
            Game game=new PokerGame("Poker",3,1,"resources\\games\\pokerGame.txt");
            // - Add players to game
            Player play1=new PokerPlayer("user1");
            Player play2=new PokerPlayer("user2");
            game.addPlayer(play1);
            game.addPlayer(play2);
            // - Run the Game loop
            Player win= game.run();
            game.displayState();
            // - Display the winner player
            System.out.println("Winner of the game is "+win.toString());
            //nettoryer les cartes de la main du gagnant
            win.clearHand();
            //supprimer les joueurs
            game.removePlayers();
        }


    }
}

